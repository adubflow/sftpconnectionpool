Sftp Connection Pool Library
============================

This production-tested, spring-framework-based library is to complement [Jsch](http://www.jcraft.com/jsch/).

Features
--------

1. Resource friendly.  Connection pool for the sftp access (1 session - pooled sftp channels).

2. Robust retry.  Exponentially retry a temporarily-downed sftp channel.

3. Recover.  After retrying 3 times and failed, recover from a disconnected sftp session.

4. Thread-safe.

5. Configurable retry and backoff policies.  See [Spring Retry](http://docs.spring.io/spring-batch/reference/html/retry.html)

Usage
-----
* Modify the sftp-connection-context.xml to your endpoint.

```
    <bean id="sessionContext" class="com.finelean.platform.connectivity.sftp.SessionContext">
        <property name="hostname" value="your_ftp_host"/>
        <property name="port" value="your_ftp_port"/>
        <property name="sshUserInfo" ref="sshUserInfo"/>
        <property name="remoteBaseDir" value="remote_base_unit_test_dir"/>
        <property name="jsch" ref="jsch"/>
        <property name="connectTimeout" value="5000"/>
        <property name="serverKeepAliveInterval" value="60000"/>
    </bean>

    <bean id="sshUserInfo" class="com.finelean.platform.connectivity.sftp.SshUserInfo">
        <constructor-arg value="your_sftp_user"/>
        <!-- keystore path takes the precedence over password -->
        <constructor-arg value="/your/key/store/path"/>
        <constructor-arg value="your_sftp_passowrd"/>
    </bean>
```

* Get an instance of remoteFileRepository

```
@Autowired
ApplicationContext applicationContext;
:
:
RemoteFileRepository remoteFileRepository =
        (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");
```

* Log file is hardcoded in log4j.xml, /var/tmp/app.log.
You may change to adapt to your own server environment, date format style, etc.

* Set up another config.xml file if you have another sftp endpoint in the same application context.

Production environment usage
----------------------------
Using password to establish a sftp connection is for unit test and illustration purpose only.

For production setup, [see here](https://bitbucket.org/finelean/sftpconnectionpool/wiki/Recommended%20Production%20Setup)

Maven repository
----------------
Coveting your feedback before I load it to the maven repository.


Getting support
---------------
Please email to simonso@yahoo.com

Building from source
--------------------

### Prerequisites
[JDK 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)

[Spring Framework](http://projects.spring.io/spring-framework/)

The project uses spring framework 4.0.
However, if you are still at 3.x, you may "tune down" by changing the version in the top-level build.gradle.

### Build a jar with dependencies.
```
./gradlew uberjar
```
Substitute the parent project with your webapp that uses the library.

### Build without dependencies
```
cd lib;../gradlew jar
```
to build a jar without dependencies.

### Import sources into your IDE
Import as a gradle project in Intellij, straight up.

Import into Eclipse, run this command and import as an existing project:
```
./gradlew cE eclipse
```

### Run unit tests
```
./gradlew test
```

License
-------
The sftpconnectionpool is released under version 2.0 of the [Apache License](http://www.apache.org/licenses/LICENSE-2.0)
