/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.app;

import com.finelean.platform.repository.RemoteFileRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This class exhibits how to use the API.
 *
 * The entry point is the RemoteFileRepository.
 *
 * Most likely you will use it with a webapp though.
 * So include spring-config/sftp-conn-context.xml in web.xml.
 *
 * @author sso
 */
public class MainApp {

    public static void main(String[] argv) {

        // please modify sftp-conn-context.xml to your endpoints before you use it!
        ApplicationContext applicationContext =
            new ClassPathXmlApplicationContext("spring-config/sftp-conn-context.xml");

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        System.out.println(remoteFileRepository.isDirectory("apps"));

    }
}