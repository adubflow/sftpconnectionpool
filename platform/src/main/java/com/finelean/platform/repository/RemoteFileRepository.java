/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.repository;

import java.io.InputStream;
import java.util.List;

/**
 * This is the interface to the client application.
 * The API hides the connection pool implementation details.
 * The API is also thread-safe.
 * The client can inject the bean and use the API straight up.
 *
 * @author sso
 */
public interface RemoteFileRepository {

    /**
     * Checks whether the path is a directory.
     *
     * @param path relative to the root directory.
     *             No forward slash
     *
     * @return true if directory exists
     */
    boolean isDirectory(final String path);

    /**
     * Checks whether the path is a file.
     *
     * @param path relative to the root directory, the location of the entity.
     * @return true if path (e.g. /app/file.txt) is a file, otherwise false.
     */
    boolean isFile(final String path);

    /**
     * Writes the content in the input stream to a file.
     * If the path doesn't exist,
     * the path will be automatically created by "mkdir -p"
     *
     * @param path
     * @param filename
     * @param srcInputStream caller must manage the input stream.
     * @return true if successful
     */
    boolean write(final String path,
                  final String filename,
                  InputStream srcInputStream);

    /**
     * Creates a file with no content.
     *
     * @param path
     * @param filename
     * @return true if the file is created.
     */
    boolean createFile(final String path,
                       final String filename);

    /**
     * Deletes a file.
     *
     * @param path
     * @param filename
     * @return true if deleted successfully.
     */
    boolean delete(final String path,
                   final String filename);

    /**
     * Lists the directory.
     * It only returns a list of names.
     * It is unlike the original jsch <code>ls</code> call,
     * which returns the permission, the date and the filename.
     *
     * @param path
     * @return a list of entries in the path.
     */
    List<String> ls(final String path);


}
