package com.finelean.platform.repository.impl;

import com.finelean.platform.connectivity.sftp.pool.PooledResource;
import com.finelean.platform.repository.RemoteFileRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.io.InputStream;
import java.util.List;

/**
 * This class is the implementation for the remote file repository.
 *
 * @author sso
 */
public class RemoteFileRepositoryImpl implements RemoteFileRepository {

    private static final Logger logger = Logger.getLogger(RemoteFileRepositoryImpl.class);

    @Autowired
    ApplicationContext applicationContext;

    private String pooledResourceName;

    @Override
    public boolean isDirectory(final String path) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.isDirectory(path);
    }

    @Override
    public boolean isFile(final String path) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.isFile(path);
    }

    @Override
    public boolean write(final String path,
                         final String filename,
                         InputStream srcInputStream) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.write(path, filename, srcInputStream);
    }

    @Override
    public boolean createFile(final String path,
                              final String filename) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.write(path, filename, null);
    }

    @Override
    public boolean delete(final String path,
                          final String filename) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.delete(path, filename);

    }

    @Override
    public List<String> ls(final String path) {
        PooledResource pooledResource =
                (PooledResource) applicationContext.getBean(pooledResourceName);

        return pooledResource.ls(path);
    }

    public String getPooledResourceName() {
        return pooledResourceName;
    }

    public void setPooledResourceName(String pooledResourceName) {
        this.pooledResourceName = pooledResourceName;
    }
}
