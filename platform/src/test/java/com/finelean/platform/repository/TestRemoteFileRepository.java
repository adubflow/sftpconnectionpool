/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.repository;

import org.apache.sshd.SshServer;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.CommandFactory;
import org.apache.sshd.server.PasswordAuthenticator;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.sftp.SftpSubsystem;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


/**
 * Unit test for the sftp repository api.
 * Unfortunately, mock sftp server (org.apache.sshd) does not work on Windows.
 * Go figure.
 *
 * @author sso
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:spring-config/test-sftp-conn-context.xml")
public class TestRemoteFileRepository {

    private static final String PLATFORM_TEST_CONTEXT_XML = "spring-config/test-sftp-conn-context.xml";

    // During the unit test, We have to create the sftp endpoint first.
    // hardcoded to match with that in test-sftp-conn-context.xml
    // As soon as the endpoint is done, the spring beans can be created.
    private static final String defaultRootDir = "remote_base_unit_test_dir";
    private static final int port = 22999;

    private static SshServer mockSshServer;

    @BeforeClass
    public static void beforeTestSetup() throws Exception {
        try {

            Path path = FileSystems.getDefault().getPath(".", defaultRootDir);
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }

            // not setting up mock server if port is < 1024.
            // the properties file may point to a real endpoint
            if (port > 1024) {
                mockSshServer = SshServer.setUpDefaultServer();
                mockSshServer.setPort(port);
                mockSshServer.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());
                mockSshServer.setPasswordAuthenticator(new PasswordAuthenticator() {
                    @Override
                    public boolean authenticate(String username, String password, ServerSession session) {
                        return true;
                    }
                });

                CommandFactory commandFactory = new CommandFactory() {
                    public Command createCommand(String command) {
                        return null;
                    }
                };
                mockSshServer.setCommandFactory(new ScpCommandFactory(commandFactory));

                List<NamedFactory<Command>> namedFactoryList = new ArrayList<>();
                namedFactoryList.add(new SftpSubsystem.Factory());
                mockSshServer.setSubsystemFactories(namedFactoryList);
                mockSshServer.start();
            }
        } catch (Exception e) {
            // panic - can't create!
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown() throws Exception {
        if (mockSshServer != null) {
            mockSshServer.stop();
        }
    }

    @Test
    public void testIsDirectory() throws Exception {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(PLATFORM_TEST_CONTEXT_XML);

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        Path path = FileSystems.getDefault().getPath(".", defaultRootDir, "apps");

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        assert(remoteFileRepository.isDirectory("apps"));

        Files.delete(path);
    }

    @Test
    public void testIsFile() throws Exception {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(PLATFORM_TEST_CONTEXT_XML);

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        // setup
        Path path = FileSystems.getDefault().getPath(".", defaultRootDir, "apps");

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        assert(!remoteFileRepository.isFile("apps"));

        // cleanup
        Process p = Runtime.getRuntime().exec("rm -rf ./" + defaultRootDir + "/apps");
        p.waitFor();
    }

    @Test
    public void testWrite() throws Exception {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(PLATFORM_TEST_CONTEXT_XML);

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        // setup
        Path path = FileSystems.getDefault().getPath(".", defaultRootDir, "apps");

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        InputStream inputStream = TestRemoteFileRepository.class.getResourceAsStream("/mycontent.txt");

        assert(remoteFileRepository.write("apps", "somecontent.txt", inputStream));

        assert(remoteFileRepository.isFile("apps/somecontent.txt"));

        // quick and dirty clean up
        Process p = Runtime.getRuntime().exec("rm -rf ./" + defaultRootDir + "/apps");
        p.waitFor();
    }

    @Test
    public void testCreateFile() throws Exception {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(PLATFORM_TEST_CONTEXT_XML);

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        // setup
        Path path = FileSystems.getDefault().getPath(".", defaultRootDir, "apps");

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        assert(remoteFileRepository.createFile("apps", "emptyfile.txt"));

        assert(remoteFileRepository.isFile("apps/emptyfile.txt"));

        // quick and dirty clean up
        Process p = Runtime.getRuntime().exec("rm -rf ./" + defaultRootDir + "/apps");
        p.waitFor();
    }

    @Test
    public void testLs() throws Exception {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(PLATFORM_TEST_CONTEXT_XML);

        RemoteFileRepository remoteFileRepository =
                (RemoteFileRepository) applicationContext.getBean("remoteFileRepository");

        // setup
        Path path = FileSystems.getDefault().getPath(".", defaultRootDir, "apps");

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }

        Path aFile = FileSystems.getDefault().getPath(".", defaultRootDir, "apps", "somefile.txt");

        Files.createFile(aFile);

        List<String> directoryListings = remoteFileRepository.ls("apps");

        assert(!directoryListings.isEmpty());

        Process p = Runtime.getRuntime().exec("rm -rf ./" + defaultRootDir + "/apps");
        p.waitFor();
    }

}
