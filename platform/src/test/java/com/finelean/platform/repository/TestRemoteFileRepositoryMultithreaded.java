package com.finelean.platform.repository;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * This class ensures the multi-threaded nature of the sftp connection pool is preserved.
 * It is excluded from "gradle test" task, because this test for the time being relies on a real sftp site
 * instead of a mock sftp site.
 * Will fix this up to use the mock sftp site later.
 * See example of using mock sftp site in <code>TestRemotefileRepository</code>
 *
 * @author sso
 * @see com.finelean.platform.repository.TestRemoteFileRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:spring-config/test-mthread-sftp-conn-context.xml")
public class TestRemoteFileRepositoryMultithreaded {

    private static final int MAX_THREADS = 100;

    @Autowired
    RemoteFileRepository remoteFileRepository;

    private ExecutorService executorService = Executors.newCachedThreadPool();

    @Test
    public void testMultithreadAccess() {

        // the config created only 10 resources in the pool.
        // here we have 30 threads.
        // make sure it won't lock.
        Runnable[] runnables = new TestRemoteFileRepositoryAccessTask[MAX_THREADS];
        int i = 0;
        Future<?>[] futures = new Future<?>[MAX_THREADS];
        for (Runnable runnable : runnables) {
            runnable = new TestRemoteFileRepositoryAccessTask(i, remoteFileRepository);
            futures[i] = executorService.submit(runnable);
            i++;
        }

        try {
            for (Future f : futures) {
                f.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // positive case not expecting exception!!!
            assert(false);
        }
    }

    @Test
    public void testMultithreadedAccessExceptionHandling() {
        // the config created only 10 resources in the pool.
        // here we have 30 threads.
        // make sure it won't lock.
        Runnable[] runnables = new TestRemoteFileRepositoryAccessTask[100];
        int i = 0;
        Future<?>[] futures = new Future<?>[100];
        for (Runnable runnable : runnables) {
            runnable = new TestFailedOperationTask(i, remoteFileRepository);
            futures[i] = executorService.submit(runnable);
            i++;
        }

        try {
            for (Future f : futures) {
                f.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
            assert(true);
        }
    }

}

class TestRemoteFileRepositoryAccessTask implements Runnable {

    private static final Logger logger = Logger.getLogger(TestRemoteFileRepositoryMultithreaded.class);

    private int index;
    private RemoteFileRepository remoteFileRepository;

    public TestRemoteFileRepositoryAccessTask(int i, RemoteFileRepository remoteFileRepository) {
        this.index = i;
        this.remoteFileRepository = remoteFileRepository;
    }

    @Override
    public void run() {
        logger.debug(String.format("at thread: %s; ls result: %d", index, remoteFileRepository.ls("apps").size()));
    }
}

class TestFailedOperationTask implements Runnable {
    private static final Logger logger = Logger.getLogger(TestRemoteFileRepositoryMultithreaded.class);

    private int index;
    private RemoteFileRepository remoteFileRepository;

    public TestFailedOperationTask(int i, RemoteFileRepository remoteFileRepository) {
        this.index = i;
        this.remoteFileRepository = remoteFileRepository;
    }

    @Override
    public void run() {
        // this will throw exception!
        logger.debug(String.format("at thread: %s; ls result: %d", index, remoteFileRepository.ls("app").size()));
    }
}